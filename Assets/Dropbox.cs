﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.SceneManagement;

public class Dropbox : MonoBehaviour
{
    Rigidbody2D rb;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void FixedUpdate()
    {
        float laserLength = 25f;
        Vector2 startPosition = (Vector2)transform.position + new Vector2(-1.9f, -1.3f);
        Vector2 endPosition = (Vector2)transform.position + new Vector2(1.9f, -1.3f);
        int layerMask = LayerMask.GetMask("Default");

        RaycastHit2D frontHit = Physics2D.Raycast(startPosition, Vector2.down, laserLength, layerMask, 0);
        RaycastHit2D endHit = Physics2D.Raycast(endPosition, Vector2.down, laserLength, layerMask, 0);

        if (frontHit)
        {
            if (frontHit.collider.tag.Equals("Player"))
            {
                Debug.Log("Hitting " + frontHit.collider.tag);
                rb.isKinematic = false;
            }
        }

        if (endHit)
        {
            if (endHit.collider.tag.Equals("Player"))
            {
                Debug.Log("Hitting " + endHit.collider.tag);
                rb.isKinematic = false;
            }
        }
        Debug.DrawRay(startPosition, Vector2.down * laserLength, Color.red);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            GameObject.Find("/GameManager").SendMessage("PlayerKilled");

        }
    }


}
