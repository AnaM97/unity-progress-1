﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class MovementNew : MonoBehaviour
{
    Rigidbody2D rb;

    public float speed_impuls = 150;
    public float jump_impuls = 45;
    public float speed_max_velocity = 50;
    public float jump_max_velocity = 55;

    public int jumps_max = 2;
    public int jumps_left = 0;

    public bool in_jump = true;
    public float jump_time = 0;
    public float jump_max_time = 0.5f;

    public bool on_ground = false;
    public bool on_left = false;
    public bool on_right = false;
    public bool on_ceiling = false;

    public Transform groundCheck;
    public LayerMask groundLayer;
    public Transform leftCheck;
    public Transform rightCheck;
    public Transform ceilingCheck;

    public Animator anim;


    // Start is called before the first frame update
    void Start()
    {
        this.resetJumps();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        // First move then check colliders, because if 
        // we first check colliders and then move, we can falsely 
        // start jump even if not allowed becase check is before the jump
        // if that makes sense, this fixes the issue where sometimes
        // player jumps heigher than usual
        Move();
        LimitVelocity();
        CheckColliders();
    }


    void CheckColliders()
    {
        on_left = on_right = on_ground = false;

        on_ground = Physics2D.OverlapBox(groundCheck.position, new Vector2(1, 1), 0, groundLayer);
        on_left = Physics2D.OverlapBox(leftCheck.position, new Vector2(1, 1), 0, groundLayer);
        on_right = Physics2D.OverlapBox(rightCheck.position, new Vector2(1, 1), 0, groundLayer);
        on_ceiling = Physics2D.OverlapBox(ceilingCheck.position, new Vector2(1, 1), 0, groundLayer);

        if ((on_ground || on_left || on_right) && in_jump)
        {
            this.resetJumps();
            jump_time = 0;
            in_jump = false;
            
        }

    }

    void Move()
    {
        int sideMovement = this.playerMovingSide();

        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetKey(KeyCode.Space) ? 1 : 0;

        if (!this.canPlayerJump())
        {
            y = 0;
        }


        if (!this.canPlayerMove(x))
        {
            x = 0;
        }

        // if changing side of movement,
        // allow faster decrese of movement 
        // until velocity side is same with wanted movment side
        if (x != sideMovement && sideMovement != 0)
        {
            x = x * 2;
        }

        // if jump
        if (y > 0) 
        {
            // record jump 
            in_jump = true;
            jumps_left--;
            jump_time = Time.time;
            anim.Play("PlayerJumpAnimation");

            // if on side, give bigger side push on jump
            if (!on_ground && x != 0)
            {
                if (on_right)
                {
                    x = -10;
                }

                if (on_left)
                {
                    x = 10;
                }
            }
        }

        if (y > 0)
        {
            rb.velocity = new Vector2(rb.velocity.x, y * this.jump_impuls);
        }

        if (x != 0)
        {
            rb.AddForce(new Vector2(x * this.speed_impuls, 0));
        }
    }

    int playerMovingSide()
    {
        if (this.rb.velocity.x > 0 && this.rb.velocity.x > 0.001f)
        {
            return 1;
        }
        else if (this.rb.velocity.x < 0 && this.rb.velocity.x < 0.001f)
        {
            return -1;
        }

        return 0;
    }

    bool canPlayerJump()
    {
        // if jumping too soon (frame by frame, etc)
        if (Time.time - jump_time < jump_max_time)
        {
            return false;
        }

        // if he cant jump any more
        if (!on_left && !on_right)
        {
            if (jumps_left == 0)
            {
                return false;

            }
        }

        // if moving to fast up, disable jump
        if (rb.velocity.y >= jump_max_velocity)
        {
            return false;
        }

        //if he's on the ceiling
        if (on_ceiling)
        {
            return false;
        } 

        return true;
    }

    bool canPlayerMove(float side)
    {
        // can't move if he is in air + colliding with 
        // wall + going against the wall
        if (!on_ground && in_jump)
        {
            if (side < 0 && on_right)
            {
                return false;
            }
            if (side > 0 && on_left)
            {
                return false;
            }
        }

        return true;
    }

    void resetJumps()
    {
        this.jumps_left = this.jumps_max;
    }

    void LimitVelocity()
    {
        float horiziontalVelocity = rb.velocity.x;
        float verticalVelocity = rb.velocity.y;

        // if moving right, and moving faster than allowed
        if (horiziontalVelocity > 0 && horiziontalVelocity > speed_max_velocity)
        {
            horiziontalVelocity = speed_max_velocity;
        }
        // if moving left and moving faster than allowed
        else if (horiziontalVelocity < 0 && horiziontalVelocity < -1 * speed_max_velocity)
        {
            horiziontalVelocity = -1 * speed_max_velocity;
        }

        // if moving up faster than allowed
        if (rb.velocity.y >= jump_max_velocity)
        {
            verticalVelocity = jump_max_velocity;
        }

        // if we changed any velocity, apply it to rigidbody
        if (horiziontalVelocity != rb.velocity.x || verticalVelocity != rb.velocity.y)
        {
            rb.velocity = new Vector2(horiziontalVelocity, verticalVelocity);
        }
    }

}
