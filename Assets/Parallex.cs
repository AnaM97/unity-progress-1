﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallex : MonoBehaviour
{
    private float startPosition;
    private float length;
    public GameObject cam;
    public float parallexEffect;
    void Start()
    {
        startPosition = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float temporaryPosition = (cam.transform.position.x * (1 - parallexEffect));
        float distance = (cam.transform.position.x * parallexEffect);

        transform.position = new Vector3(startPosition + distance, transform.position.y, transform.position.z);

        if (temporaryPosition > startPosition + length)
        {
            startPosition += length;
        } else if (temporaryPosition < startPosition - length) 
        {
            startPosition -= length;
        }
    }
}
