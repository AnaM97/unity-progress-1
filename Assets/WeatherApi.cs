﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class WeatherApi : MonoBehaviour
{
    public string key = "2e9d1707160d52b1445455bbd8e6e6a9";
    public string cityId = "792680";
    private Quaternion rotation;

    public void Start()
    {
        rotation = gameObject.transform.rotation;
        setWeather();
    }

    IEnumerator GetRequest(string url, Action<UnityWebRequest> callback)
    {
        using (UnityWebRequest request = UnityWebRequest.Get(url))
        {
            // Send the request and wait for a response
            yield return request.SendWebRequest();
            callback(request);
        }

    }
    public void setWeather()
    {
        string url = "https://api.openweathermap.org/data/2.5/weather?id=" + cityId + "&appid=" + key;

        Debug.Log(url);
        StartCoroutine(GetRequest(url, (UnityWebRequest req) =>
        {
            if (req.isNetworkError || req.isHttpError)
            {
                Debug.Log($"{req.error}: {req.downloadHandler.text}");
            }
            else
            {
                Debug.Log(req.downloadHandler.text);
                Weather weather = JsonUtility.FromJson<Weather>(req.downloadHandler.text);
                Debug.Log("deg: " + weather.wind.deg);
                gameObject.transform.rotation = new Quaternion(rotation.x, rotation.y, weather.wind.deg, rotation.w);
                ParticleSystem ps = gameObject.GetComponent<ParticleSystem>();
                var vel = ps.velocityOverLifetime;
                vel.speedModifierMultiplier = 5 * weather.wind.speed;
            }

          
        }));
    }
}

[Serializable]
class Weather
{
    public string name;
    public Wind wind;
}

[Serializable]
class Wind
{
    public float speed;
    public float deg;
}