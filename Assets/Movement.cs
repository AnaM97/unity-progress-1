﻿using UnityEngine;

public class Movement : MonoBehaviour
{
    Rigidbody2D rb;

    public float speedImpuls = 5;
    public float jumpImpuls = 10;
    public float maxSpeed = 50;
    public float maxJump = 40;

    
    public bool in_jump = true;
    public float jump_time = 0;
    public float max_jump_time = 0.1f;

    public bool on_ground = false;
    public bool on_left = false;
    public bool on_right = false;
    public bool on_ceiling = false;

    public Transform groundCheck;
    public LayerMask groundLayer;
    public Transform leftCheck;
    public Transform rightCheck;
    public Transform ceilingCheck;

    public bool space = false;
    public int maxJumps = 2;
    public int jumpsLeft;
    

    // Start is called before the first frame update
    void Start()
    {
        jumpsLeft = maxJumps;
        rb = GetComponent<Rigidbody2D>(); 
    }

    // Update is called once per frame
    
    void FixedUpdate()
    {
        CheckColliders();
        Move();
    }

    void CheckColliders()
    {
        on_left = on_right = on_ground = on_ceiling = false;

        on_ground = Physics2D.OverlapBox(groundCheck.position, new Vector2(1, 1), 0, groundLayer);
        on_left = Physics2D.OverlapBox(leftCheck.position, new Vector2(1, 1), 0, groundLayer);
        on_right = Physics2D.OverlapBox(rightCheck.position, new Vector2(1, 1), 0, groundLayer);
        on_ceiling = Physics2D.OverlapBox(ceilingCheck.position, new Vector2(1, 1), 0, groundLayer);

        if (on_ground && in_jump)
        {
            in_jump = false;
            jumpsLeft = maxJumps;
            jump_time = 0;
        }
        /*
        if (on_ceiling)
        {
            in_jump = true;
            can_jump = false;
        }
        */
    }

    void Move()
    {
        int xSide = rb.velocity.x > 0 ? 1 : -1;
        float x = Input.GetAxisRaw("Horizontal");
        float y = Input.GetKeyDown(KeyCode.Space) ? 1 : 0;

        if (!on_left && !on_right)
        {
            if (jumpsLeft == 0 && in_jump)
            {
                y = 0;
            }
        }
 

        if (rb.velocity.y >= maxJump * maxJumps)
        {
            y = 0;
            rb.velocity = new Vector2(rb.velocity.x, maxJump * maxJumps);
        }

        if (Time.time - jump_time < 0.1) 
        {
            y = 0;
        }

        if (y > 0)
        {
            //zapamti vreme skoka i dozvoli da skoci sa zida
            in_jump = true;
            jump_time = Time.time;
            jumpsLeft--;
        }

        if (x != xSide) 
        {
            x = x * 2;
        }

        if (Mathf.Abs(rb.velocity.x) >= maxSpeed)
        {
            

            if (x == xSide)
            {
                x = 0;
                rb.velocity = new Vector2(maxSpeed * xSide, rb.velocity.y);
            }
        }
        


        if (y > 0) //ako je hteo da skoci
        {
            y = 10;
            if (on_right)
            {
                x = -10;
            }

            if (on_left)
            {
                x = 10;
            }
        }

        if (x == 0 && y == 0) 
        {
            return;
        }

       
        Vector2 impulse = new Vector2(x * this.speedImpuls, y*this.jumpImpuls);
        rb.AddForce(impulse);
    }
}
